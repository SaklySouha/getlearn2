import React from 'react';
import { MDBBtn, MDBCard,MDBFormInline,MDBCardBody,MDBIcon, MDBCardImage, MDBCardTitle,
   MDBCardText, MDBCol, MDBContainer, MDBRow } from 'mdbreact';
import "./PageCoursDev.css";


const PageCoursDev = () => {
 
  return (
     
      <section className="body-pagecours" >
   <MDBContainer>
       <MDBCol md="12" className="mb-4">
        <MDBCard className="card-image" style={{
                backgroundImage:
                  "url(https://mdbootstrap.com/img/Photos/Others/img%20%2832%29.jpg)"
              }}>
          <div className="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4 rounded">
            <div>
             
              <h3 className="py-3 font-weight-bold">
                <strong>Cours dev</strong>
              </h3>
              <p className="pb-3">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Repellat fugiat, laboriosam, voluptatem, optio vero odio
                nam sit officia accusamus minus error nisi architecto
                nulla ipsum dignissimos. Odit sed qui, dolorum!
              </p>
              <MDBBtn color="secondary" rounded size="md">
                <MDBIcon far icon="clone" className="left" /> MDBView project
              </MDBBtn>
            </div>
          </div>
        </MDBCard>
      </MDBCol>
      <MDBCol md="6">
      <MDBFormInline className="md-form">
        <MDBIcon icon="search" />
        <input className="form-control form-control-sm ml-3 w-75" type="text" placeholder="Search" aria-label="Search" />
      </MDBFormInline>
    </MDBCol>
   
    <MDBCol md="12" className="mb-4" >  
      <MDBRow >


      <MDBCard style={{ width: "22rem" }} className="cours-card">
        <MDBCardImage className="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" waves />
        <MDBCardBody>
          <MDBCardTitle>Node js</MDBCardTitle>
          <MDBCardText>
            Some quick example text to build on the card title and make
            up the bulk of the card&apos;s content.
          </MDBCardText>
          <MDBBtn href="./PageCoursNode">Consulter </MDBBtn>
        </MDBCardBody>
      </MDBCard>

      

      



    
      <MDBCard style={{ width: "22rem" }} className="cours-card">
        <MDBCardImage className="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" waves />
        <MDBCardBody>
          <MDBCardTitle>React js</MDBCardTitle>
          <MDBCardText>
            Some quick example text to build on the card title and make
            up the bulk of the card&apos;s content.
          </MDBCardText>
          <MDBBtn href="./PageCoursNode">Consulter </MDBBtn>
        </MDBCardBody>
      </MDBCard>
      


      


      

      <MDBCard style={{ width: "22rem" }} className="cours-card">
        <MDBCardImage className="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" waves />
        <MDBCardBody>
          <MDBCardTitle>PHP</MDBCardTitle>
          <MDBCardText>
            Some quick example text to build on the card title and make
            up the bulk of the card&apos;s content.
          </MDBCardText>
          <MDBBtn href="./PageCoursNode">Consulter </MDBBtn>
        </MDBCardBody>
      </MDBCard>
  
      </MDBRow>

      
</MDBCol>


    </MDBContainer>
    </section>   
  )
}

export default PageCoursDev;