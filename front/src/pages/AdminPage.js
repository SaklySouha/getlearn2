import React from "react";
import { MDBContainer, MDBRow, MDBCol, MDBBtn } from 'mdbreact';
import axios from "axios";
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import "./AdminPage.css";

class AdminPage extends React.Component {

 constructor(props){

     super(props);

this.onChangeNameF=this.onChangeNameF.bind(this);
this.onChangedefF=this.onChangedefF.bind(this);
this.onChangecategF=this.onChangecategF.bind(this);
this.onChangenameFormatF=this.onChangenameFormatF.bind(this);
this.onChangeprixF=this.onChangeprixF.bind(this);
this.onChangecreated=this.onChangecreated.bind(this);
this.onSubmit=this.onSubmit.bind(this);


     this.state = {
         nameF:"",
         defF:"",
         categF:"",
         nameFormatF:"",
         prixF:0,
         created:new Date(),
         
     }
 }

componentDidMount(){
    this.setState({
        
    })
}


 onChangeNameF(e){
     this.setState({
        nameF:e.target.value
     });
 }

 onChangedefF(e){
    this.setState({
       defF:e.target.value
    });
}

onChangecategF(e){
    this.setState({
       categF:e.target.value
    });
}

onChangenameFormatF(e){
    this.setState({
       nameFormatF:e.target.value
    });
}

onChangeprixF(e){
    this.setState({
       prixF:e.target.value
    });
}

onChangecreated(date){
    this.setState({
       created:date
    });
}


onSubmit(e){
    e.preventDefault();

    const formation = {
        nameF:this.state.nameF,
        defF:this.state.defF,
        categF:this.state.categF,
        nameFormatF:this.state.nameFormatF,
        prixF:this.state.prixF,
        created:this.state.created

    }
    console.log(formation);
    axios.post('http://localhost:5000/api/v1/formation',formation)
    .then(res => console.log(res.data));

    window.location='/PageCoursDev';
}


 render() {
return (
<MDBContainer className="Admin-page">
  <MDBRow>
    <MDBCol md="12" >
      <form onSubmit={this.onSubmit}>
        <p className="h4 text-center mb-4">Créer votre formation</p>
        <label htmlFor="defaultFormRegisterNameEx" className="grey-text">
Nom    </label>

        <input type="text" id="defaultFormRegisterNameEx" className="form-control" />
        <br />
        <label htmlFor="defaultFormRegisterNameEx" className="grey-text">
Définition        </label>
        <input type="text" id="defaultFormRegisterNameEx" className="form-control" value={this.state.defF} onChange={this.onChangedefF}/>
        <br />
        <label htmlFor="defaultFormRegisterNameEx" className="grey-text">
Catégorie        </label>
        <input type="text" id="defaultFormRegisterNameEx" className="form-control" value={this.state.categF} onChange={this.onChangecategF} />
        <br />
        <label htmlFor="defaultFormRegisterNameEx" className="grey-text">
Nom de formateur    </label>
        <input type="text" id="defaultFormRegisterNameEx" className="form-control" value={this.state.nameFormatF} onChange={this.onChangenameFormatF} />
        <label htmlFor="defaultFormRegisterNameEx" className="grey-text">
Prix   </label>
        <input type="Number" id="defaultFormRegisterNameEx" className="form-control" value={this.state.prixF} onChange={this.onChangeprixF} />

        <label htmlFor="defaultFormRegisterNameEx" className="grey-text">
Date de création   </label>
        < DatePicker selected={this.state.created} onChange={this.created} />




        <div className="text-center mt-4">
          <MDBBtn color="unique" type="submit">
            Partager
          </MDBBtn>
        </div>
      </form>
    </MDBCol>
  </MDBRow>
</MDBContainer>
);
}
};

export default AdminPage;